import React from 'react';
import style from '../styles/components/header.module.scss';
const Header = () => {
  return (
    <div className={style.header}>
      <div className="logo">
        <img src={require('../../public/images/HackLab.png')} alt="logo" />
      </div>
    </div>
  );
};

export default Header;
