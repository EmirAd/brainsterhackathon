import React, { useEffect, useState } from 'react';
import style from '../styles/adminForm.module.scss';

import Link from 'next/link';
import Teams from './Teams';
import { TeamType } from '../types/type';
import { useRouter } from 'next/router';

interface Props {}
const AdminRegister = () => {
  const [password, setPassword] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [show, setShow] = useState<boolean>(false);
  const [name, setName] = useState<string>('');
  const router = useRouter();
  const handleSubmit = (e: any) => {
    const regobj = { name, password, email };
    fetch('http://localhost:5001/admin', {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify(regobj),
    });
    console.log(regobj);
    router.push('/');
  };
  return (
    <div className={style.admin}>
      <form className={style.adminForm}>
        <div className={style.banner}>
          <p>Sign Up</p>
        </div>
        <div className={style.wrapper}>
          <label htmlFor="name">Name and Surname</label>
          <input
            required
            type="text"
            onChange={(e) => {
              setName(e.target.value);
            }}
            value={name}
          />
          <label htmlFor="email">Email</label>
          <input
            required
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          <label htmlFor="password">Password</label>
          <div className={style.pass}>
            <input
              required
              type={show === true ? 'text' : 'password'}
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
            <span>
              <i
                onClick={() => {
                  setShow(!show);
                }}
                className={
                  show === false
                    ? 'fa-solid fa-eye'
                    : 'fa-sharp fa-solid fa-eye-slash'
                }
              ></i>
            </span>
          </div>
          <Link href={'/'}>
            <button
              type="submit"
              onClick={(e) => {
                handleSubmit(e);
              }}
            >
              Sign Up
            </button>
          </Link>
        </div>
      </form>
    </div>
  );
};

export default AdminRegister;
