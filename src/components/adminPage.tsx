import React from 'react';
import style from '../styles/components/admin.module.scss';
import Teams from './Teams';
import { TeamType } from '../types/type';
interface Props {
  teamsData: TeamType[];
}
const AdminPage = ({ teamsData }: Props) => {
  return (
    <div className={style.admin}>
      <div className={style.sidebar}>
        <h3>Project Name</h3>
        <h5>Project X</h5>

        <h3>Event Link</h3>
        <button>Get link</button>

        <h3>Location</h3>
        <h5>Brainster</h5>

        <h3>Start Date</h3>
        <h5>05/13/2023</h5>

        <h3>End Date</h3>
        <h5>07/13/2023</h5>
      </div>
      <div className={style.wrapper}>
        <div className={style.main}>
          <div className={style.section1}>
            <div className={style.applicants}>
              <p>Current Number of Applicants </p>

              <h2>80</h2>

              <p>Minimum number: 10</p>
            </div>
            <div className={style.presence}>
              <h3>Presence</h3>
              <p>In person</p>

              <div className={style.bar}>
                <div className={style.progress}>
                  <p>56</p>
                </div>
              </div>

              <p>Online</p>

              <div className={style.bar}>
                <div className={style.progress}>
                  <p>56</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Teams teamsData={teamsData} />
      </div>
    </div>
  );
};

export default AdminPage;
