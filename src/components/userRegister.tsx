import React, { useState } from 'react';
import { AcademyData } from '../types/type';
import style from '../styles/components/userRegister.module.scss';
import Image from 'next/image';
interface Props {
  academiesData: AcademyData[];
}
const UserRegister = ({ academiesData }: Props) => {
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [agree, setAgree] = useState<string>('');
  const [participate, setParticipate] = useState<string>('');
  const [alergy, setAlergy] = useState<string>('');
  const [alergic, setAlergic] = useState<string>('');
  const [preference, setPreference] = useState<string>('');
  const [additional, setAditional] = useState<string>('');
  const [registered, setRegistered] = useState<boolean>(false);

  const clearAll = () => {
    setName('');
    setEmail('');
    setPhone('');
    setAgree('');
    setAlergy('');
    setAlergic('');
    setParticipate('');
    setPreference('');
    setAditional('');
  };
  return (
    <div className={style.user}>
      {registered === false ? (
        <form
          onSubmit={() => {
            setRegistered(true);
          }}
        >
          <div className={style.banner}>
            <p>Registration form</p>
          </div>
          <div className={style.banner2}>
            <h3>Project Name</h3>
          </div>
          <div className={style.wrapper}>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Accusantium ratione deserunt distinctio non ullam nobis quia vero
              culpa atque inventore veritatis eos similique soluta impedit quod
              rem nihil hic magni vel nostrum sit et, omnis tempora! Laudantium
              nisi id pariatur aliquam accusantium earum iure eos. Aspernatur
              cum eveniet corporis eos.
            </p>
            <div>
              <label htmlFor="name">Name and Surname *</label>
              <input
                type="text"
                required
                id="name"
                value={name}
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
            </div>
            <div>
              <label htmlFor="email">Email *</label>
              <input
                type="email"
                id="email"
                required
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </div>
            <div>
              <label htmlFor="phone">Phone Number *</label>
              <input
                type="tel"
                required
                id="phone"
                value={phone}
                onChange={(e) => {
                  setPhone(e.target.value);
                }}
              />
            </div>

            <p>
              Do you agree to 48 hours of commitment, work and availability
              during the hackathon? *
            </p>
            <div className={style.flex}>
              <input
                type="radio"
                id="yes"
                name="option"
                checked={agree === 'yes' ? true : false}
                required
                onClick={() => {
                  setAgree('yes');
                }}
              />
              <label htmlFor="yes">Yes</label>
            </div>
            <div className={style.flex}>
              <input
                type="radio"
                id="no"
                name="option"
                checked={agree === 'no' ? true : false}
                onClick={() => {
                  setAgree('no');
                }}
              />
              <label htmlFor="no">No</label>
            </div>

            <p>Which academy do you come from? *</p>
            <select name="academy" id="academy" required>
              {academiesData.map((academy, index) => (
                <option value="academy" key={index}>
                  {academy.academy}
                </option>
              ))}
            </select>
            <p>Will you participate online or in person? *</p>
            <div className={style.flex}>
              <input
                type="radio"
                id="online"
                name="option2"
                required
                checked={participate === 'online' ? true : false}
                onClick={() => {
                  setParticipate('online');
                }}
              />
              <label htmlFor="online">Online</label>
            </div>
            <div className={style.flex}>
              <input
                type="radio"
                id="live"
                name="option2"
                checked={participate === 'live' ? true : false}
                onClick={() => {
                  setParticipate('live');
                }}
              />
              <label htmlFor="live">In person</label>
            </div>

            <p>
              Food preferences: <span>&#40;optional&#41;</span>
            </p>
            <div className={style.flex}>
              <input
                type="radio"
                id="vegan"
                name="option3"
                checked={preference === 'vegan' ? true : false}
                onClick={() => {
                  setPreference('vegan');
                }}
              />
              <label htmlFor="vegan">Vegan</label>
            </div>
            <div className={style.flex}>
              <input
                type="radio"
                id="vegetarian"
                name="option3"
                checked={preference === 'vegetarian' ? true : false}
                onClick={() => {
                  setPreference('vegetarian');
                }}
              />
              <label htmlFor="vegetarian">Vegetarian</label>
            </div>
            <div className={style.flex}>
              <input
                type="radio"
                id="none"
                name="option3"
                checked={preference === 'none' ? true : false}
                onClick={() => {
                  setPreference('none');
                }}
              />
              <label htmlFor="none">None</label>
            </div>

            <p>Allergies</p>
            <div className={style.flex}>
              <input
                type="radio"
                name="option4"
                id="not"
                required
                checked={alergic === 'not' ? true : false}
                onClick={() => {
                  setAlergic('not');
                }}
              />
              <label htmlFor="not">None</label>
            </div>
            <div className={style.flex}>
              <input
                type="radio"
                name="option4"
                id="allergic"
                checked={alergic === 'alergic' ? true : false}
                onClick={() => {
                  setAlergic('alergic');
                }}
              />
              <label htmlFor="allergic">Yes</label>
              <input
                className={style.alergy}
                type="text"
                required={alergic === 'alergic' ? true : false}
                value={alergy}
                onChange={(e) => {
                  setAlergy(e.target.value);
                }}
              />
            </div>

            <p>Anything additional?</p>
            <input
              type="text"
              value={additional}
              onChange={(e) => {
                setAditional(e.target.value);
              }}
            />

            <div className={style.btns}>
              <button type="submit">Submit</button>
              <button
                type="button"
                className={style.clear}
                onClick={() => {
                  clearAll();
                }}
              >
                Clear Form
              </button>
            </div>
          </div>
        </form>
      ) : (
        ''
      )}
      {registered && (
        <div className={style.thnk}>
          <div className={style.logo}></div>
          <h4>Your Application was Successful!! </h4>
          <p>Will come back to you with further details</p>
        </div>
      )}
    </div>
  );
};

export default UserRegister;
