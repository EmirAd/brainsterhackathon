import React, { useState } from 'react';
import { TeamType } from '../types/type';
import style from '../styles/components/teams.module.scss';
interface Props {
  teamsData: TeamType[];
}
const Teams = ({ teamsData }: Props) => {
  const [accordion, setAccordion] = useState<number>(-1);

  function toggleAccordion(index: any) {
    if (index === accordion) {
      setAccordion(-1);
      return;
    }
    setAccordion(index);
  }

  return (
    <div className={style.teams}>
      <h4>View Teams </h4>

      {teamsData.map((team, index) => (
        <div
          className={style.item}
          key={index}
          onClick={() => {
            toggleAccordion(index);
          }}
        >
          <div className={style.heading}>
            <h3 className={accordion === index ? style.active : ''}>
              {team.team}
            </h3>
            <span>
              <i
                className={
                  accordion === index
                    ? 'fa-solid fa-chevron-up'
                    : 'fa-solid fa-chevron-down'
                }
              ></i>
            </span>
          </div>

          {accordion === index && (
            <table className={style.table}>
              <thead>
                <th>Name and Surname</th>
                <th>Email address</th>
                <th>Role</th>
                <th>Group</th>
                <th>Presence</th>
              </thead>
              <tbody>
                {team.members.map((member, idx) => (
                  <tr key={idx}>
                    <td>{member.name}</td>
                    <td>{member.email}</td>
                    <td>{member.role}</td>
                    <td>{member.group}</td>
                    <td>{member.presence}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          )}
        </div>
      ))}
    </div>
  );
};

export default Teams;
