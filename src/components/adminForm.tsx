import React, { useEffect, useState } from 'react';
import style from '../styles/adminForm.module.scss';

import Link from 'next/link';
import Teams from './Teams';
import { TeamType } from '../types/type';
import AdminPage from './adminPage';

interface Props {
  teamsData: TeamType[];
}
const AdminForm = ({ teamsData }: Props) => {
  const [logedIn, setLogedIn] = useState<boolean>(false);
  const LS_LOGIN = 'LS_LOGIN';
  const [password, setPassword] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [show, setShow] = useState<boolean>(false);

  const [error, setError] = useState<boolean>(false);
  useEffect(() => {
    const data = window.localStorage.getItem(LS_LOGIN);
    if (data !== null) {
      setLogedIn(JSON.parse(data));
    }
  }, []);

  useEffect(() => {
    window.localStorage.setItem(LS_LOGIN, JSON.stringify(logedIn));
    console.log(logedIn);
  }, [logedIn]);

  const validate = () => {
    let result = true;
    if (email === '' || email === null) {
      alert('fill the form');
      result = false;
    }
    if (password === '' || password === null) {
      alert('fill the form');
      result = false;
    }
    return result;
  };
  const login = () => {
    if (validate()) {
      fetch('http://localhost:5001/admin?q=' + email)
        .then((res) => {
          return res.json();
        })
        .then((resp) => {
          if (Object.keys(resp).length === 0) {
            setError(true);
          } else {
            if (resp[0].password === password) {
              setLogedIn(true);
              console.log('success');
            } else {
              console.log('fail');
            }
          }
          console.log(resp[0].password);
        });
    }
  };
  return (
    <div className={style.admin}>
      {logedIn === false && (
        <form
          className={style.adminForm}
          onSubmit={(e) => {
            login();
            e.preventDefault();
          }}
        >
          <div className={style.banner}>
            <p>Sign In</p>
          </div>
          <div className={style.wrapper}>
            <label htmlFor="email">Email</label>
            <input
              required
              type="email"
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
              }}
            />
            <label htmlFor="password">Password</label>
            <div className={style.pass}>
              <input
                required
                type={show === true ? 'text' : 'password'}
                value={password}
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
              <span>
                <i
                  onClick={() => {
                    setShow(!show);
                  }}
                  className={
                    show === false
                      ? 'fa-solid fa-eye'
                      : 'fa-sharp fa-solid fa-eye-slash'
                  }
                ></i>
              </span>
            </div>
            {error && <p className={style.error}>Wrong Username or Password</p>}
            <button type="submit">Sign in</button>

            <Link href={'/adminSignup'}>Sign Up</Link>
          </div>
        </form>
      )}
      {logedIn === true && <AdminPage teamsData={teamsData} />}
    </div>
  );
};

export default AdminForm;
