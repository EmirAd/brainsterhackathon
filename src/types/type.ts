export interface AcademyData {
  academy: string;
}

export interface TeamType {
  team: string;
  members: [
    {
      name: string;
      email: string;
      role: string;
      academy: string;
      group: string;
      presence: string;
    }
  ];
}
