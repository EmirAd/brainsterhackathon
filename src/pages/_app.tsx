import '../styles/bootstrap.min.css';
import '../styles/util.css';
import '../styles/main.css';
import type { AppProps } from 'next/app';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
